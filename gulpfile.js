'use strict';

const   gulp            = require('gulp'),
        mainBowerFiles  = require('main-bower-files'),
        prefixer        = require('gulp-autoprefixer'),
        sass            = require('gulp-sass'),
        imagemin        = require('gulp-imagemin'),
        pngquant        = require('imagemin-pngquant'),
        rimraf          = require('rimraf'),
        browserSync     = require("browser-sync"),
        pug             = require('gulp-pug'),
        uncss           = require('gulp-uncss'),
        reload          = browserSync.reload;

let path = {
    vendor: {
        js: 'app/js/vendor/',
        css: 'app/css/vendor/',
        fonts: 'app/fonts/vendor'
    },
    dist: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'dist/',
        pug: 'dist/',
        js: 'dist/js/',
        sass: 'dist/css/',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/',
        webfonts: 'dist/webfonts/'
    },
    app: { //Пути откуда брать исходники
        html: 'app/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        pug: 'app/*.pug', //Синтаксис src/*.pug говорит gulp что мы хотим взять все файлы с расширением .pug
        js: 'app/js/*.js', //В стилях и скриптах нам понадобятся только main файлы
        sass: 'app/css/*.scss',
        css: 'app/css/*.css',
        img: 'app/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'app/fonts/**/*.*',
        webfonts: 'app/webfonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'app/**/*.html',
        pug: 'app/**/*.pug',
        pugSpec: 'app/specification/**/*.pug',
        pugSection: 'app/section/**/*.pug',
        js: 'app/js/**/*.js',
        sass: 'app/css/**/*.scss',
        sassSpec: 'app/css/specification/**/*.scss',
        sassSection: 'app/css/section/**/*.scss',
        css: 'app/css/**/*.css',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },
    clean: './dist'
};

let config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: false,
    host: 'localhost',
    port: 8081,
    logPrefix: "Verkt"
};

gulp.task('uncssCss', function () {
    return gulp.src('dist/css/main.css')
        .pipe(uncss({
            html: ['dist/*.html']
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('uncssBootstrap', function () {
    return gulp.src('dist/css/bootstrap.min.css')
        .pipe(uncss({
            html: ['dist/*.html']
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('uncss', gulp.parallel(
    'uncssBootstrap',
    'uncssCss'
));

gulp.task('vendorJs:build', function () {
    return gulp.src( mainBowerFiles('**/*.js') ) //Выберем файлы по нужному пути
        .pipe(gulp.dest(path.vendor.js)) //Выплюнем готовый файл в app
        .pipe(gulp.dest(path.dist.js)) //Выплюнем готовый файл в dist
});

gulp.task('vendorCss:build', function () {
    return gulp.src( mainBowerFiles('**/*.css') ) //Выберем файлы по нужному пути
        .pipe(gulp.dest(path.vendor.css)) //И в app
        .pipe(gulp.dest(path.dist.css)) //И в dist
});

gulp.task('icon:build', function () {
    return gulp.src( mainBowerFiles('**/*+(.woff2|woff|ttf|svg|eot)') )
        .pipe(gulp.dest(path.dist.webfonts)) //И в dist
});

gulp.task('html:build', function () {
    return gulp.src(path.app.html) //Выберем файлы по нужному пути
        .pipe(gulp.dest(path.dist.html)) //Выплюнем их в папку dist
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
    return gulp.src(path.app.js) //Найдем наш main файл
        .pipe(gulp.dest(path.dist.js)) //Выплюнем готовый файл в dist
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('sass:build', function () {
    return gulp.src(path.app.sass) //Выберем наш main.scss
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(gulp.dest(path.dist.sass)) //И в dist
        .pipe(reload({stream: true}));
});

gulp.task('pug:build', function () {
    return gulp.src(path.app.pug) //Выберем наши pug файлы
        .pipe(pug({
            pretty: true
          }))
        .pipe(gulp.dest(path.dist.pug)) //И в dist
        .pipe(reload({stream: true}));
});

gulp.task('css:build', function () {
    return gulp.src(path.app.css) //Выберем наш main.css
        .pipe(gulp.dest(path.dist.css)) //И в dist
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    return gulp.src(path.app.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.dist.img)) //И бросим в dist
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    return gulp.src(path.app.fonts)
        .pipe(gulp.dest(path.dist.fonts))
});

gulp.task('build', gulp.series(
    'vendorCss:build',
    'vendorJs:build',
    'html:build',
    'pug:build',
    'js:build',
    'sass:build',
    'css:build',
    'fonts:build',
    'image:build'
));

gulp.task('watch', function() {
    gulp.watch(path.watch.html, gulp.series('html:build'));
    gulp.watch(path.watch.pug, gulp.series('pug:build'));
    gulp.watch(path.watch.pugSpec, gulp.series('pug:build'));
    gulp.watch(path.watch.pugSection, gulp.series('pug:build'));
    gulp.watch(path.watch.sass, gulp.series('sass:build'));
    gulp.watch(path.watch.sassSpec, gulp.series('sass:build'));
    gulp.watch(path.watch.sassSection, gulp.series('sass:build'));
    gulp.watch(path.watch.css, gulp.series('css:build'));
    gulp.watch(path.watch.js, gulp.series('js:build'));
    gulp.watch(path.watch.img, gulp.series('image:build'));
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', gulp.series('build', gulp.parallel('webserver', 'watch')));